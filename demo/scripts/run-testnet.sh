#!/bin/bash

set -eux

N=${1:-4}
MPWD=$(pwd)

docker network create \
  --driver=bridge \
  --subnet=172.78.0.0/16 \
  --ip-range=172.78.5.0/24 \
  --gateway=172.78.5.254 \
  v2vnet

for i in $(seq 1 $N)
do
    docker create --name=node$i --net=v2vnet --ip=172.78.5.$i v2vnetworks/v2v-core run \
    --cache_size=50000 \
    --tcp_timeout=200 \
    --heartbeat=10 \
    --node_addr="172.78.5.$i:1337" \
    --proxy_addr="172.78.5.$i:1338" \
    --client_addr="172.78.5.$(($N+$i)):1339" \
    --service_addr="172.78.5.$i:80" \
    --sync_limit=500
    docker cp $MPWD/conf/node$i node$i:/.v2v-core
    docker start node$i

    docker run -d --name=client$i --net=v2vnet --ip=172.78.5.$(($N+$i)) -it v2vnetworks/dummy \
    --name="client $i" \
    --client_addr="172.78.5.$(($N+$i)):1339" \
    --proxy_addr="172.78.5.$i:1338" \
    --log_level="info"  
done

docker run --name=watcher --net=v2vnet --ip=172.78.5.$(($N+$N+1)) -it v2vnetworks/watcher
